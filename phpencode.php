<?php
function utf8($txt) {
  $encoding = mb_detect_encoding($txt, 'ASCII,UTF-8,ISO-8859-1');
  if ($encoding == "ISO-8859-1") {
     $txt = utf8_encode($txt);
  }
  return $txt;
}
$fgc = file_get_contents('product.csv');
// echo $fgc;
echo utf8($fgc);
