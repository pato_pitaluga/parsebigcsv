const fs = require('fs');
const path = require('path');
const rimraf = require('rimraf');

const partialToSubpartial = 6;

const lineReader = require('readline').createInterface({
  input: require('fs').createReadStream('partials/partial-' + partialToSubpartial + '/consultants.csv', {
    encoding: 'latin1'
  })
});

let countLines = 0;
let countFiles = 1;
console.log(countFiles);
lineReader.on('line', (line) => {
  countLines++;

  if (!fs.existsSync('partials/partial-' + partialToSubpartial + '/subpartial-' + countFiles)) {
    fs.mkdirSync('partials/partial-' + partialToSubpartial + '/subpartial-' + countFiles);
  }

  if ((line.match(/¬/g) || []).length === 34) {
    fs.appendFile('partials/partial-' + partialToSubpartial + '//subpartial-' + countFiles + '/consultants.csv', line + "\n", {
      encoding: 'latin1'
      // encoding: 'utf8'
    }, (err) => {
       if (err) throw err;
    });
    if (countLines % 1 === 0) {
      // process.exit(1); // For debugging
      countFiles++;
      console.log(countFiles);
    }
  } else {
    console.log('.' + countLines, line);
  }

});
