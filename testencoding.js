const lineReader = require('readline').createInterface({
  input: require('fs').createReadStream('product.csv', {
    // encoding: 'utf8',
    // encoding: 'ascii', // Works better but the é is readed as i and other vowels the same.
    // encoding: 'utf16le', // Wonderfull error
    // encoding: 'ucs2', // Wonderfull error
    // encoding: 'base64', // NOT
    encoding: 'latin1',
    // encoding: 'binary',
    // encoding: 'hex',
  })
});

lineReader.on('line', (line) => {
  console.log(line);
});
