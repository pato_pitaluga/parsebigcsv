const fs = require('fs');
const path = require('path');
const rimraf = require('rimraf');

fs.readdir('partials', (err, files) => {
  if (err) throw err;

  const howManyFiles = files.length;
  let count = 0;
  for (const file of files) {
    count++;
    if (file.substr(0, 7) === 'partial') {
      rimraf.sync('./partials/' + file);
    }
    if (count === howManyFiles) startParse();
  }
});

function startParse() {
  const lineReader = require('readline').createInterface({
    input: require('fs').createReadStream('consultants.csv', {
      // encoding: 'utf8',
      // encoding: 'ascii', // Works better but the é is readed as i and other vowels the same.
      // encoding: 'utf16le', // Wonderfull error
      // encoding: 'ucs2', // Wonderfull error
      // encoding: 'base64', // NOT
      encoding: 'latin1',
      // encoding: 'binary',
      // encoding: 'hex',
    })
  });

  let countLines = 0;
  let countFiles = 0;
  lineReader.on('line', (line) => {
    countLines++;
    // line = line.replace(/�/g, '", "')
    // line = line.replace(/¬/g, '", "') // Is this one

    // line = line.replace(new RegExp(String.fromCharCode(65533), 'g'), '", "');
    // fs.appendFile('partials/partial-' + countFiles + '.csv', '"' + line + '"' +"\n", (err) => {

    if (!fs.existsSync('partials/partial-' + countFiles)) {
      fs.mkdirSync('partials/partial-' + countFiles);
    }

//     line = unescape(encodeURIComponent(line));
//     line = line.replace(`
// `, ' ');
//     line = line.replace(/(\r\n|\n|\r)/gm, ' ');
//     line = line.replace(/  /, ' ');
//     line = line.replace(/\u000A/, ' ');
//     line = decodeURIComponent(escape(line));

    // To export ready to debug in google docs:
    // line = line.replace(/,/g, '\\,');
    // line = '"' + line.replace(/¬/g, '","') + '"';

    if ((line.match(/¬/g) || []).length === 34) {
      fs.appendFile('partials/partial-' + countFiles + '/consultants.csv', line + "\n", {
        encoding: 'latin1'
        // encoding: 'utf8'
      }, (err) => {
         if (err) throw err;
      });
      if (countLines % 1500 === 0) {
        // process.exit(1); // For debugging
        countFiles++;
        console.log(countFiles);
      }
    } else {
      console.log('.' + countLines, line);
    }

  });
}
